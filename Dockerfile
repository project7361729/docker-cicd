FROM python:3
ENV PYTHONUNBUFFERED 1
WORKDIR ./job

COPY . /job/

RUN pip install -r requirements.txt

VOLUME ["job/db"]
EXPOSE 8000
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000
